# README #

My Diary is a android based basic application developed for the beginners.

### What is this repository for? ###

This repository is to provide a clear view of the basic concepts of android skill development.

### How do I get set up? ###

This repo is open source. You can simply clone the project and use as your own

### Contribution guidelines ###

Creator: Md. Al- Zihad
         CSEDU, University of Dhaka

### Who do I talk to? ###

Mail me al: alzihadtiash@gmail.com