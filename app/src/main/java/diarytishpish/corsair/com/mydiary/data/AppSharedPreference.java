package diarytishpish.corsair.com.mydiary.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Tiash on 7/13/2016.
 */public class AppSharedPreference
{
    private static AppSharedPreference mAppSharedPreference;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private static Context mContext;
    /*
     * Implementing Singleton DP
     */
    private AppSharedPreference() {
        mSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        mEditor = mSharedPreferences.edit();
    }

    public static AppSharedPreference getInstance(Context context) {
        mContext = context;
        if (mAppSharedPreference == null)
            mAppSharedPreference = new AppSharedPreference();
        return mAppSharedPreference;
    }

    // operation type defines if adding story is a new insert or a update of a previous one
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public void putOperationType(Boolean type)
    {
        mEditor.putBoolean("type",type);
        mEditor.commit();
    }
    public boolean getOperationType() {
        return mSharedPreferences.getBoolean("type", true);  // true means operation
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void putCurrentDataToEdit(String title,String description, String cover, String date, String genre,Boolean fav)
    {
        mEditor.putString("title", title);
        mEditor.putString("description", description);
        mEditor.putString("cover", cover);
        mEditor.putString("date", date);
        mEditor.putString("genre", genre);
        mEditor.putBoolean("fav",fav);
        mEditor.commit();
    }

    public Data getCurrentDataToEdit()
    {
        Data data = new Data(mSharedPreferences.getString("title", ""),mSharedPreferences.getString("description", ""),
                mSharedPreferences.getString("cover", ""),mSharedPreferences.getString("genre", ""),
                mSharedPreferences.getString("date", ""),mSharedPreferences.getBoolean("fav",false));
        return data;
    }


    // changing page from scrollung activity to category info
    // needed data = category name + category cover

    public void put_category_name_and_cover(String name,String cover)
    {
        mEditor.putString("category_name",name);
        mEditor.putString("category_cover",cover);
        mEditor.commit();
    }

    public String get_category_name()
    {
        return mSharedPreferences.getString("category_name","");
    }
    public String get_category_cover()
    {
        return mSharedPreferences.getString("category_cover","error");
    }


    /////////////////////////////////////////////////////////////////////////
    // get sett lock status

    public void seLock(String lock)
    {
        mEditor.putString("lock",lock);
        mEditor.commit();
    }
    public String getLock()
    {
        return mSharedPreferences.getString("lock","none");
    }


}