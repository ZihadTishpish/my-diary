package diarytishpish.corsair.com.mydiary.splash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import diarytishpish.corsair.com.mydiary.R;
import diarytishpish.corsair.com.mydiary.ScrollingActivity;

/**
 * Created by Tiash on 7/10/2016.
 */
public class SplashScreen extends Activity
{
    int splashDelay = 500;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);


        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        //set manual height and width to the logo

        createTables();
       // ImageView logo = (ImageView) findViewById(R.id.logo);
       // logo.setMaxWidth((width*2)/3);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/card.ttf");
        TextView tv = (TextView) findViewById(R.id.intro_text);
        tv.setTypeface(type);
        YoYo.with(Techniques.Swing).duration(500).playOn(tv);
        // Adding a Timer to exit splash screen
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {


                    startActivity(new Intent(SplashScreen.this, ScrollingActivity.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

            }
        }, splashDelay);
    }

    public void createTables()
    {
        db = openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS data(title VARCHAR PRIMARY KEY NOT NULL,description VARCHAR,genre VARCHAR,date VARCHAR,cover VARCHAR,favourite VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS genre(title VARCHAR PRIMARY KEY NOT NULL,count INT,image VARCHAR);");

        try {
      //     db.execSQL("INSERT INTO genre VALUES('GenreName','Count','imageName');");
            db.execSQL("INSERT INTO genre VALUES('Food','0','food');");
            db.execSQL("INSERT INTO genre VALUES('Travel','0','travel');");
            db.execSQL("INSERT INTO genre VALUES('Mind','0','mind');");
            db.execSQL("INSERT INTO genre VALUES('Friends','0','friends');");
        }
        catch (Exception e)
        {

        }

    }
}


/*
    db.execSQL("INSERT INTO quick VALUES('"+e1.getText()+"','"+e2.getText()+"');");
    db.execSQL("UPDATE quick SET rollno='"+e1.getText().toString()+"',name='"+e2.getText().toString()+"' WHERE rollno='"+name+"'");
    Cursor c=db.rawQuery("SELECT * FROM quick WHERE rollno='"+item.getname()+"'", null);

    private void fillTheString()
	{
		Cursor c=db.rawQuery("SELECT * FROM quick", null);
		//showMessage("Sx","TRhdty"+c.getCount());

		//int i=0;
		if(c.getCount()==0)
		{
			names[0]="no resource";
			dcNames[0]=" ";
			//time[0]=" ";
			return;
		}

		//c.moveToFirst();
		while(c.moveToNext())
		{
			names[total]=c.getString(0);
			dcNames[total]=c.getString(1);
			//time[total]=c.getString(2);
			total++;
		}/*
		names[0]= "tias";
		dcNames[0]="hihi";

}




        */