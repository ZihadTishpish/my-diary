package diarytishpish.corsair.com.mydiary.data;

/**
 * Created by Tiash on 7/10/2016.
 */
public class Genre
{
    private String count,genre,image;

    public Genre (String genre, String count,String image)
    {
        this.genre = genre;
        this.count = count;
        this.image = image;

    }
    public void setGenre(String genre)
    {
        this.genre = genre;
    }
    public String getGenre() {   return genre ;  }
    public void setCount(String count) { this.count = count; }
    public String getCount() {   return count ;  }

    public void setImage(String image) { this.image = image; }
    public String getImage() {   return image ;  }

}
