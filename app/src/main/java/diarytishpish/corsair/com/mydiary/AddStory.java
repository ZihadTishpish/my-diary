package diarytishpish.corsair.com.mydiary;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import java.util.ArrayList;
import java.util.List;

import diarytishpish.corsair.com.mydiary.adapters.CardForMainPageAdapter;
import diarytishpish.corsair.com.mydiary.adapters.StoryCoverAdapter;
import diarytishpish.corsair.com.mydiary.data.AppSharedPreference;
import diarytishpish.corsair.com.mydiary.data.Data;
import diarytishpish.corsair.com.mydiary.data.Genre;

/**
 * Created by Tiash on 7/10/2016.
 */
public class AddStory extends AppCompatActivity
{
    RelativeLayout switch_edit;
    DatePicker pick_date;
    EditText title_edit,description_text;
    TextView title,intro_text,title_text,date_date,date_desc;
    CardView genre, genre_list, date_card, date_picker_card, title_card, save_card;
    ImageView close,cover, date_close, date_select;
    RecyclerView main_card, main_card2;
    List<Genre> card_item = new ArrayList<>();
    List<String> cover_image_list = new ArrayList<>();
    StoryCoverAdapter storyCoverAdapter;
    CardForMainPageAdapter cardAdapter;
    CollapsingToolbarLayout collapsingToolbarLayout;
    SharedPreferences sharedPreferences;
    AppBarLayout appBarLayout;
    LinearLayout draft_page;
    SQLiteDatabase db;
    Boolean operationType = true;
    boolean fav_value;
    View view;
    // true means insert operation

    String selected_genre="none", selected_date="none", selected_title="none", selected_description="none", selected_cover="none", previous_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_story_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        save_card = (CardView) findViewById(R.id.save_card);
        draft_page = (LinearLayout) findViewById(R.id.draft_page);
        appBarLayout = ( AppBarLayout) findViewById(R.id.app_bar);
        sharedPreferences = getSharedPreferences("boolean",Context.MODE_PRIVATE);
        main_card2 = (RecyclerView) findViewById(R.id.cover_list);
        main_card = (RecyclerView) findViewById(R.id.card_category);
        genre = (CardView) findViewById(R.id.genre);
        date_card = (CardView) findViewById(R.id.date);
        title_card = (CardView) findViewById(R.id.title_card);
        date_picker_card = (CardView) findViewById(R.id.datepickercard);
        genre_list = (CardView) findViewById(R.id.genre_list);
        close = (ImageView) findViewById(R.id.close);
        date_close = (ImageView) findViewById(R.id.date_close);
        date_select = (ImageView) findViewById(R.id.date_select);
        pick_date = (DatePicker) findViewById(R.id.pick_date);
        title = (TextView) findViewById(R.id.title);
        title_text = (TextView) findViewById(R.id.title_text);
        description_text = (EditText) findViewById(R.id.description_text);
        cover = (ImageView) findViewById(R.id.cover);
        intro_text = (TextView) findViewById(R.id.intro_text);
        title_edit = (EditText) findViewById(R.id.title_edit);
        date_date = (TextView) findViewById(R.id.date_date);
        date_desc = (TextView) findViewById(R.id.date_desc);


        set_Current_Date();
        get_Genre_From_Database();
        checkIfStoryIsNew();

        intro_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                select_Story_Cover();
            }
        });

        collapsingToolbarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                setTitle();
                cover_image_list = new ArrayList<>();
                select_Story_Cover();
            }
        });

        appBarLayout.addOnOffsetChangedListener(
                new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(collapsingToolbarLayout.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout)) {
                   // genre.animate().alpha(0).setDuration(600);
                    genre.animate().rotationX(-90f).setDuration(500);
                    genre.animate().rotationY(0f).setDuration(500);

                    date_card.animate().rotationX(-90f).setDuration(500);
                    date_card.animate().rotationY(0f).setDuration(500);

                    genre_list.animate().rotationX(0f).setDuration(1000);
                    genre_list.animate().rotationY(0f).setDuration(1000);

                    if (!selected_title.matches("none"))
                        collapsingToolbarLayout.setTitle("Edit "+ selected_title);
                    else
                        collapsingToolbarLayout.setTitle("Add Story");



                } else {
                    //genre.animate().alpha(1).setDuration(600);
                    // when the app bar is open
                    genre.animate().rotationX(15f).setDuration(1000);
                    date_card.animate().rotationX(15f).setDuration(1000);

                    genre.animate().rotationY(15f).setDuration(1000);
                    date_card.animate().rotationY(-15f).setDuration(1000);

                    if (!selected_title.matches("none"))
                        collapsingToolbarLayout.setTitle(selected_title);
                    else
                        collapsingToolbarLayout.setTitle("");

                }
            }
        });


        genre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setTitle();
                genre.setVisibility(View.GONE);
                date_card.setVisibility(View.GONE);
               // intro_text.setVisibility(View.GONE);
                genre_list.setVisibility(View.VISIBLE);
                card_item = new ArrayList<>();
                in_It_Category();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genre_list.setVisibility(View.GONE);
                date_card.setVisibility(View.VISIBLE);
                genre.setVisibility(View.VISIBLE);
                //   intro_text.setVisibility(View.VISIBLE);
            }
        });

        date_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_picker_card.setVisibility(View.GONE);
                date_card.setVisibility(View.VISIBLE);
                genre.setVisibility(View.VISIBLE);
                //   intro_text.setVisibility(View.VISIBLE);
            }
        });

        date_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Make_Change_To_Date(pick_date.getDayOfMonth(),pick_date.getMonth(),pick_date.getYear());
                date_picker_card.setVisibility(View.GONE);
                date_card.setVisibility(View.VISIBLE);
                genre.setVisibility(View.VISIBLE);
            }
        });

        date_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                setTitle();
                date_picker_card.setVisibility(View.VISIBLE);
                genre.setVisibility(View.GONE);
                genre_list.setVisibility(View.GONE);
                date_card.setVisibility(View.GONE);
            }
        });

        title_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                get_Title_Input();
            }
        });

        description_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTitle();
                appBarLayout.setExpanded(false);
            }
        });

        save_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                selected_description = description_text.getText().toString();
                selected_title = title_edit.getText().toString();
                setTitle();
                if (
                        !selected_cover.matches("none")&&
                        !selected_date.matches("none")&&
                        !selected_genre.matches("none")&&
                        !selected_title.matches("none")&&
                        !selected_description.matches("none")&&
                        !selected_title.matches("")&&
                        !selected_description.matches("")
                        )
                {
                /*    Toast.makeText(getApplicationContext(), selected_cover+"  "+
                            selected_genre+" "+selected_title+" "+selected_description+" "+
                            selected_date,Toast.LENGTH_LONG).show();*/
                 //   db.execSQL("CREATE TABLE IF NOT EXISTS data(
                    // title VARCHAR,
                    // description VARCHAR,
                    // genre VARCHAR,
                    // date VARCHAR,
                    // favourite VARCHAR);");

                    if (operationType) {
                        db.execSQL("INSERT INTO data VALUES('" + selected_title +
                                "','" + selected_description +
                                "','" + selected_genre +
                                "','" + selected_date +
                                "','" + selected_cover +
                                "','false');"); // false is for favourite
                        startActivity(new Intent(getApplicationContext(), ScrollingActivity.class));
                    }

                    else
                    {
                        //db.execSQL("UPDATE quick SET rollno='"+e1.getText().toString()+"',name='"+e2.getText().toString()+"' WHERE rollno='"+name+"'");

                        db.execSQL("UPDATE data SET title ='"+
                                selected_title+"',description='"+
                                selected_description+"',genre='"+
                                selected_genre+"',date='"+
                                selected_date+"',cover='"+
                                selected_cover+"',favourite='"+fav_value+"' WHERE title='"+previous_title+"'"); // false is for favourite
                        startActivity(new Intent(getApplicationContext(), ScrollingActivity.class));

                       // Toast.makeText(getApplicationContext(),"update koira dimu, khara :p ",Toast.LENGTH_LONG).show();
                    }
                }

                else
                {
                    Toast.makeText(getApplicationContext(),"Please enter all the fields",Toast.LENGTH_LONG).show();
                }

            }
        });




    }



    public void checkIfStoryIsNew()
    {


        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        Data data = asp.getCurrentDataToEdit();
        String story_title = data.getTitle();
        String story_description = data.getDescription();
        String story_cover = data.getCover();
        String genre = data.getGenre();
        String date = data.getDate();
        fav_value = data.getFavourite();
        if (!story_title.matches("null"))
        {
            selected_title = story_title;
            title_edit.setText(story_title);
            title_text.setText(story_title);
            previous_title = story_title;
            setTitle();
            operationType = false; // this means "update" operation is needed
        }

        if (!story_description.matches("null"))
        {
            selected_description = story_description;
            description_text.setText(story_description);
        }

        if (!story_cover.matches("null"))
        {
            selected_cover = story_cover;
            int id = getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +story_cover , null, null);
            collapsingToolbarLayout.setBackgroundResource(id);
        }
        if (!genre.matches("null"))
        {
            selected_genre = genre;
            for (int i=0;i<card_item.size();i++)
            {
                Genre gen = card_item.get(i);
                Log.d("genre",gen.getGenre()+ "   "+ genre);
                if (gen.getGenre().matches(genre))
                {
                    Log.d("match","matched");
                    changeGenre(gen);
                }
            }
        }

        if (!date.matches("null"))
        {
            String string[]= date.split(" ");
            if (string.length==3)
            {
                String day = string[0];
                String month = string[1];
                String year = string[2];

                Make_Change_To_Date(Integer.parseInt(day),
                                    Integer.parseInt(month),
                                    Integer.parseInt(year));
            }
        }

    }


    public void set_Current_Date()
    {
        Make_Change_To_Date(pick_date.getDayOfMonth(),pick_date.getMonth(),pick_date.getYear());
    }

    public  void get_Title_Input()
    {
        title_edit.setVisibility(View.VISIBLE);
        if (!title_text.getText().toString().matches("Story Title"))
        {
            title_edit.setText(title_text.getText().toString());
        }
        title_text.setVisibility(View.GONE);
    }

    public void setTitle()
    {
         if (title_text.getVisibility()== View.GONE)
        {
            String text = title_edit.getText().toString();
            if (text.length()>0)
            {
                title_edit.setVisibility(View.GONE);
                title_text.setVisibility(View.VISIBLE);
                title_text.setText(text);
                selected_title = text; // for saving purpose
            }
            else
            {
                title_edit.setVisibility(View.GONE);
                title_text.setVisibility(View.VISIBLE);
                title_text.setText("Story Title");
            }

        }
    }

    public void Make_Change_To_Date(int day, int Month, int Year)
    {
        String Month_name[]={"January","February","March","April","May","June","July","August","September","October","November","December"};
        date_date.setText(" "+day+" ");
        date_desc.setText(Month_name[Month]+", "+Year);
        selected_date = day+" "+Month+" "+Year; // saving purpose
    }



    public void select_Story_Cover()
    {
       // genre.setVisibility(View.GONE);
        intro_text.setVisibility(View.GONE);
        //genre_list.setVisibility(View.VISIBLE);

        YoYo.with(Techniques.SlideOutLeft).duration(500).playOn(genre);
        YoYo.with(Techniques.SlideOutRight).duration(500).playOn(date_card);
        main_card2.setVisibility(View.VISIBLE);
        card_item = new ArrayList<>();
        in_It_Cover_Choser();
    }

    public void in_It_Cover_Choser()
    {

        get_Cover_Image_From_Database();
        LinearLayoutManager mymanager = new LinearLayoutManager(getApplicationContext());
        storyCoverAdapter = new StoryCoverAdapter(cover_image_list,getApplicationContext());
        mymanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        main_card2.setLayoutManager(mymanager);
        main_card2.setItemAnimator(new DefaultItemAnimator());
        main_card2.setAdapter(storyCoverAdapter);
        storyCoverAdapter.notifyDataSetChanged();
        main_card2.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), main_card2, new ScrollingActivity.ClickListener() {
            @Override
            public void onClick(View view, int position)
            {
                String image = cover_image_list.get(position);
                selected_cover = image; // saving purpose
                int id = getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +image , null, null);
                collapsingToolbarLayout.setBackgroundResource(id);
                main_card2.setVisibility(View.GONE);
                YoYo.with(Techniques.SlideInLeft).duration(500).playOn(genre);
                YoYo.with(Techniques.SlideInRight).duration(500).playOn(date_card);

            }

            @Override
            public void onLongClick(View view, int position)
            {
                Toast.makeText(getApplicationContext(),"long",Toast.LENGTH_SHORT).show();
            }
        }));
    }

    public void get_Cover_Image_From_Database()
    {
        cover_image_list.add("food");
        cover_image_list.add("friends");
        cover_image_list.add("mind");
        cover_image_list.add("travel");
        cover_image_list.add("mind");
        cover_image_list.add("travel");
    }

    public void in_It_Category()
    {
        get_Genre_From_Database();
        LinearLayout mainpageaudio = (LinearLayout) findViewById(R.id.main_page_card);
        LinearLayoutManager mymanager = new LinearLayoutManager(getApplicationContext());
        cardAdapter = new CardForMainPageAdapter(card_item,getApplicationContext());
        mymanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        main_card.setLayoutManager(mymanager);
        main_card.setItemAnimator(new DefaultItemAnimator());
        main_card.setAdapter(cardAdapter);
        cardAdapter.notifyDataSetChanged();
        main_card.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), main_card, new ScrollingActivity.ClickListener() {
            @Override
            public void onClick(View view, int position)
            {
                Genre select = card_item.get(position);
                changeGenre(select);
                genre_list.setVisibility(View.GONE);
                genre.setVisibility(View.VISIBLE);
                date_card.setVisibility(View.VISIBLE);

                //  intro_text.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLongClick(View view, int position)
            {
                Toast.makeText(getApplicationContext(),"long",Toast.LENGTH_SHORT).show();
            }
        }));
    }



    public void get_Genre_From_Database()
    {
        SQLiteDatabase db = openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
        Cursor cursor=db.rawQuery("SELECT * FROM genre", null);

        if (cursor.getCount()>0)
        {
            while(cursor.moveToNext())
            {
                String genre =cursor.getString(0);
                String count =cursor.getString(1);
                String image = cursor.getString(2);
                card_item.add(new Genre(genre,count+" stories",image));
            }
        }


    }

    public void changeGenre (Genre genre)
    {
        title.setText(genre.getGenre());
        selected_genre = genre.getGenre(); // saving purpose
        int id = getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +genre.getImage() , null, null);
        cover.setImageResource(id);
    }

    @Override
    public void onBackPressed()
    {
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.confirm_dialog, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width-200,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);

        Button cancel, discard;
        cancel = (Button) view.findViewById(R.id.cancel);
        discard = (Button) view.findViewById(R.id.discard);
        discard.setWidth((width-250) /2);
        cancel.setWidth((width-250)/2);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vw)
            {
                YoYo.with(Techniques.TakingOff).duration(500)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                windowManager.removeViewImmediate(view);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).playOn(view);

            }
        });
        discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vi) {
                YoYo.with(Techniques.TakingOff).duration(500)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                windowManager.removeViewImmediate(view);
                                startActivity(new Intent(getApplicationContext(),ScrollingActivity.class));
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).playOn(view);
            }
        });
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;
        windowManager.addView(view, params);
        YoYo.with(Techniques.BounceInLeft).duration(500).playOn(view);

    }

}
