package diarytishpish.corsair.com.mydiary;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.takwolf.android.lock9.Lock9View;

import java.util.ArrayList;
import java.util.List;

import diarytishpish.corsair.com.mydiary.adapters.CardForMainPageAdapter;
import diarytishpish.corsair.com.mydiary.adapters.FavouriteForMainPageAdapter;
import diarytishpish.corsair.com.mydiary.adapters.StoryCoverChoserAdapter;
import diarytishpish.corsair.com.mydiary.data.AppSharedPreference;
import diarytishpish.corsair.com.mydiary.data.Data;
import diarytishpish.corsair.com.mydiary.data.Genre;

public class ScrollingActivity extends AppCompatActivity 
{
    LinearLayout favourite_layout;
    private long mLastClickTime = 0;
    TextView tias_save;
    EditText tiasText;
    String selected_image = "null";
    ImageView tias_cover,close_dlog,lock_icon;
    CardView add_genre,lock;
    RecyclerView main_card, fav_card;
    List<Genre> card_item = new ArrayList<>();
    List<Data> fav_item = new ArrayList<>();
    CardForMainPageAdapter cardAdapter;
    FavouriteForMainPageAdapter favAdapter;
    List<String> cover_image_list = new ArrayList<>();
    StoryCoverChoserAdapter storyCoverAdapter;
    View view;
    int type = 1; // type 1 = set password, type 2 = remove password, type 3 = confirm before read
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lock = (CardView) findViewById(R.id.lock);
        main_card = (RecyclerView) findViewById(R.id.card_category);
        add_genre = (CardView) findViewById(R.id.add_genre);
        fav_card = (RecyclerView) findViewById(R.id.card_fav);
        lock_icon = (ImageView) findViewById(R.id.lock_icon);
        favourite_layout = (LinearLayout) findViewById(R.id.favourite_layout);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                asp.putCurrentDataToEdit("null","null","null","null","null",false);
               startActivity(new Intent(getApplicationContext(),AddStory.class));
            }
        });

        add_genre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                YoYo.with(Techniques.Swing).duration(500).playOn(add_genre);
                take_Input();
            }
        });
        in_It_Category();
        in_It_Favourite();
        LockStatusUpdate();
        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                CheckLock();
            }
        });
    }

    public void CheckLock()
    {
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.lock_screen, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width-200,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);
        YoYo.with(Techniques.FadeIn).duration(500).playOn(view);
        Lock9View lock9View = (Lock9View) view.findViewById(R.id.lock_9_view);
        TextView pattern_text = (TextView) view.findViewById(R.id.pattern_title);
        TextView forgot = (TextView) view.findViewById(R.id.forgot);
        if (type==1)
        {
            forgot.setVisibility(View.GONE);
            pattern_text.setText("Enter a Security Pattern");
        }
        else if (type ==2)
        {
            forgot.setVisibility(View.VISIBLE);
            pattern_text.setText("Enter Previous Pattern");
        }
        else if (type ==3 || type == 4)
        {
            forgot.setVisibility(View.VISIBLE);
            pattern_text.setText("Enter Security Pattern");
        }
        lock9View.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password)
            {
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                if (type ==1)
                {
                    type =2;
                    asp.seLock(password);
                    lock_icon.setImageResource(R.mipmap.lock);
                    windowManager.removeViewImmediate(view);
                    Toast.makeText(getApplicationContext(),"You are secured",Toast.LENGTH_SHORT).show();
                }
                else if (type == 2)
                {
                    if (password.matches(asp.getLock()))
                    {
                        type = 1;
                        asp.seLock("none");
                        lock_icon.setImageResource(R.mipmap.unlock);
                        windowManager.removeViewImmediate(view);
                        Toast.makeText(getApplicationContext(), "Lock Removed", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        windowManager.removeViewImmediate(view);
                        Toast.makeText(getApplicationContext(), "Incorrect Pattern", Toast.LENGTH_SHORT).show();
                    }
                }
                else if (type ==3)
                {
                    if (password.matches(asp.getLock()))
                    {
                        startActivity(new Intent(getApplicationContext(), CategoryData.class));
                        windowManager.removeViewImmediate(view);
                    }
                    else
                    {
                        LockStatusUpdate();
                        windowManager.removeViewImmediate(view);
                        Toast.makeText(getApplicationContext(), "Incorrect Pattern", Toast.LENGTH_LONG).show();
                    }
                }

                else if (type ==4)
                {
                    if (password.matches(asp.getLock()))
                    {
                        startActivity(new Intent(getApplicationContext(),ReadStory.class));
                        windowManager.removeViewImmediate(view);
                    }
                    else
                    {
                        LockStatusUpdate();
                        windowManager.removeViewImmediate(view);
                        Toast.makeText(getApplicationContext(), "Incoreect Pattern", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;
        windowManager.addView(view, params);

    }
    public void LockStatusUpdate()
    {
        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        String value = asp.getLock();
        if (value.matches("none"))
        {
            type=1;
            lock_icon.setImageResource(R.mipmap.unlock);
        }
        else
        {
            type=2;
            lock_icon.setImageResource(R.mipmap.lock);
        }
    }

    private void take_Input()
    {

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
       int width = displayMetrics.widthPixels;

        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.take_genre_input_card, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width-200,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);

        tias_cover = (ImageView) view.findViewById(R.id.tias_cover);
        close_dlog = (ImageView) view.findViewById(R.id.close_dlog);
        tias_save = (TextView) view.findViewById(R.id.genre_save);
        tiasText = (EditText) view.findViewById(R.id.input);

        RecyclerView main_Card2 = (RecyclerView) view.findViewById(R.id.card_category);
        in_It_Cover_Choser(main_Card2);

        close_dlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                YoYo.with(Techniques.ZoomOut).duration(500)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                windowManager.removeViewImmediate(view);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).playOn(view);
            }
        });

        tias_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tiasText.getText().toString().length()>0 && !selected_image.matches("null"))
                {

                   SQLiteDatabase db = openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
                    try
                    {
                        db.execSQL("INSERT INTO genre VALUES('"+tiasText.getText().toString()+"','0','"+selected_image+"');");
                        card_item = new ArrayList<>();
                        in_It_Category();
                        cardAdapter.notifyDataSetChanged();

                        YoYo.with(Techniques.ZoomOut).duration(500)
                                .interpolate(new AccelerateDecelerateInterpolator())
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        windowManager.removeViewImmediate(view);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                }).playOn(view);
                    }
                    catch (Exception e)
                    {
                        Log.d("error ki?", e.toString());
                        Toast.makeText(getApplicationContext(),"Category Already Available",Toast.LENGTH_LONG).show();
                    }
                }

                else
                {
                    Toast.makeText(getApplicationContext(),"Please Fill all the Fields",Toast.LENGTH_LONG).show();
                }

            }
        });

        YoYo.with(Techniques.FadeIn).duration(500).playOn(view);

        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;
        windowManager.addView(view, params);

    }

    public void in_It_Category()
    {
        LinearLayout mainpageaudio = (LinearLayout) findViewById(R.id.main_page_card);

        get_Genre_From_Database();

        LinearLayoutManager mymanager = new LinearLayoutManager(getApplicationContext());
        cardAdapter = new CardForMainPageAdapter(card_item,getApplicationContext());
        mymanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        main_card.setLayoutManager(mymanager);
        main_card.setItemAnimator(new DefaultItemAnimator());
        main_card.setAdapter(cardAdapter);
        cardAdapter.notifyDataSetChanged();
        main_card.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), main_card, new ClickListener() {
            @Override
            public void onClick(View view, int position)
            {


                Genre select = card_item.get(position);
                // Toast.makeText(getApplicationContext(),select.getGenre(),Toast.LENGTH_SHORT).show();
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                asp.put_category_name_and_cover(select.getGenre(),select.getImage());

                YoYo.with(Techniques.Swing).duration(500).interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                                if (asp.getLock().matches("none"))
                                {
                                    startActivity(new Intent(getApplicationContext(), CategoryData.class));
                                }
                                else
                                {
                                    type = 3;
                                    CheckLock();
                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).playOn(view.findViewById(R.id.dekhi));

            }

            @Override
            public void onLongClick(View view, int position)
            {
            //    Toast.makeText(getApplicationContext(),"long",Toast.LENGTH_SHORT).show();
            }
        }));


    }

    public void in_It_Favourite()
    {
        LinearLayout mainpageaudio = (LinearLayout) findViewById(R.id.main_fav_card);

        getFavouriteStories();
     /*  9+29
               021/441111-*/

        LinearLayoutManager mymanager = new LinearLayoutManager(getApplicationContext());
        favAdapter = new FavouriteForMainPageAdapter(fav_item,getApplicationContext());
        mymanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        fav_card.setLayoutManager(mymanager);
        fav_card.setItemAnimator(new DefaultItemAnimator());
        fav_card.setAdapter(favAdapter);
        favAdapter.notifyDataSetChanged();
        fav_card.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), fav_card, new ClickListener() {
            @Override
            public void onClick(View view, int position)
            {
                /*String select = fav_item.get(position).getTitle();
                Toast.makeText(getApplicationContext(),select,Toast.LENGTH_SHORT).show();*/
                Data data = fav_item.get(position);
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                asp.putCurrentDataToEdit(data.getTitle(),data.getDescription(),data.getCover(),data.getDate(),data.getGenre(),data.getFavourite());

                YoYo.with(Techniques.Swing).duration(500)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation)
                            {
                                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                                if (asp.getLock().matches("none"))
                                {
                                    startActivity(new Intent(getApplicationContext(),ReadStory.class));
                                }
                                else
                                {
                                    type = 4;
                                    CheckLock();
                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).playOn(view.findViewById(R.id.dekhi));
            }

            @Override
            public void onLongClick(View view, int position)
            {
                Toast.makeText(getApplicationContext(),"long",Toast.LENGTH_SHORT).show();
            }
        }));

        if (fav_item.size()==0)
        {
            favourite_layout.setVisibility(View.GONE);
        }
    }

    private void getFavouriteStories()
    {
        SQLiteDatabase db = openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
        Cursor cursor=db.rawQuery("SELECT * FROM data", null);
        if (cursor.getCount()>0)
        {
            while(cursor.moveToNext())
            {
                // title VARCHAR,
                // description VARCHAR,
                // genre VARCHAR,
                // date VARCHAR,
                // favourite VARCHAR);");

                String title =cursor.getString(0);
                String description =cursor.getString(1);
                String genre = cursor.getString(2);
                String date =cursor.getString(3);
                String cover = cursor.getString(4);
                String fav = cursor.getString(5);
                if (fav.matches("true"))
                    fav_item.add(new Data(title, description, cover, genre, date, true));
                }
                // card_item.add(new Genre(genre,count+" stories",image));
        }

    }


    public void get_Genre_From_Database()
    {
        SQLiteDatabase db = openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
        Cursor cursor=db.rawQuery("SELECT * FROM genre", null);

        if (cursor.getCount()>0)
        {
            while(cursor.moveToNext())
            {
                int count_num =0;
                String genre =cursor.getString(0);
                String count =cursor.getString(1);
                String image = cursor.getString(2);
                Cursor cur= db.rawQuery("SELECT * FROM data Where genre ='"+genre+"'", null);
                count = ""+cur.getCount();

                card_item.add(new Genre(genre,count+" stories",image));
            }
        }


    }

    public interface ClickListener
    {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public void in_It_Cover_Choser(RecyclerView main_card2)
    {

        get_Cover_Image_From_Database();
        LinearLayoutManager mymanager = new LinearLayoutManager(getApplicationContext());
        storyCoverAdapter = new StoryCoverChoserAdapter(cover_image_list,getApplicationContext());
        mymanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        main_card2.setLayoutManager(mymanager);
        main_card2.setItemAnimator(new DefaultItemAnimator());
        main_card2.setAdapter(storyCoverAdapter);
        storyCoverAdapter.notifyDataSetChanged();
        main_card2.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), main_card2, new ScrollingActivity.ClickListener() {
            @Override
            public void onClick(View view, int position)
            {
                String name = cover_image_list.get(position);
                int id = getApplicationContext().getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +name , null, null);
                tias_cover.setImageResource(id);
                selected_image = name;
            }

            @Override
            public void onLongClick(View view, int position)
            {
                Toast.makeText(getApplicationContext(),"long",Toast.LENGTH_SHORT).show();
            }
        }));
    }

    public void get_Cover_Image_From_Database()
    {
        cover_image_list.add("food");
        cover_image_list.add("friends");
        cover_image_list.add("mind");
        cover_image_list.add("travel");
        cover_image_list.add("mind");
        cover_image_list.add("travel");
    }

    @Override
    public void onBackPressed()
    {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000)
        {
            finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        Toast.makeText(getApplicationContext(), "Tap Again to Exit", Toast.LENGTH_LONG).show();
        return;
    }





}

