package diarytishpish.corsair.com.mydiary.adapters;
import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

import diarytishpish.corsair.com.mydiary.R;
import diarytishpish.corsair.com.mydiary.data.Data;
import diarytishpish.corsair.com.mydiary.data.Genre;

public class CardForMainPageAdapter extends RecyclerView.Adapter<CardForMainPageAdapter.MyViewHolder> {

    private List<Genre> AudioForMainPagesList;
    private Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        ImageView songCover;

        public MyViewHolder(View view)
        {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.genre);
            songCover = (ImageView) view.findViewById(R.id.audiocover);
        }
    }

    public CardForMainPageAdapter(List<Genre> AudioForMainPagesList, Context context)
    {
        this.AudioForMainPagesList = AudioForMainPagesList;
        this.context = context;
    //    Toast.makeText(context,"inside",Toast.LENGTH_LONG).show();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_item_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        String AudioForMainPage = AudioForMainPagesList.get(position).getGenre();
        holder.title.setText(AudioForMainPage);
        holder.count.setText(AudioForMainPagesList.get(position).getCount());
        int id = context.getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +AudioForMainPagesList.get(position).getImage() , null, null);
        holder.songCover.setImageResource(id);
        holder.itemView.animate().rotationY(3).setDuration(250);
    }

    @Override
    public int getItemCount() {
        return AudioForMainPagesList.size();
    }
}
