package diarytishpish.corsair.com.mydiary;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import diarytishpish.corsair.com.mydiary.data.AppSharedPreference;
import diarytishpish.corsair.com.mydiary.data.Data;

/**
 * Created by Tiash on 7/18/2016.
 */
public class ReadStory extends AppCompatActivity
{
    AppSharedPreference asp;
    TextView title,description,date,genre;
    String title_text;
    ImageView like;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    boolean like_satus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.read_story_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                 startActivity(new Intent(getApplicationContext(),AddStory.class));
            }
        });
        /// in it
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        date = (TextView) findViewById(R.id.date);
        genre = (TextView) findViewById(R.id.category);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);

        /// get data from previous page
        asp = AppSharedPreference.getInstance(getApplicationContext());
        Data data = asp.getCurrentDataToEdit();

        // initialise data
        title.setText(data.getTitle());
        title_text = data.getTitle();
        description.setText(data.getDescription());
        genre.setText(data.getGenre());
        date.setText(make_date(data.getDate()));
        like_satus = data.getFavourite();
        // control the bar
        appBarLayout.addOnOffsetChangedListener(
                new AppBarLayout.OnOffsetChangedListener() {
                    @Override
                    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                        if(collapsingToolbarLayout.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout)) {
                            // genre.animate().alpha(0).setDuration(600);

                                collapsingToolbarLayout.setTitle(title_text);



                        } else {
                            //genre.animate().alpha(1).setDuration(600);
                            // when the app bar is open

                                collapsingToolbarLayout.setTitle("");

                        }
                    }
                });
        //like unlike control





    }




    public String make_date (String date)
    {
        String text=" ";
        String string[]= date.split(" ");
        if (string.length==3)
        {
            String day = string[0];
            String month = string[1];
            String year = string[2];

            int y = Integer.parseInt(month);
            String Month_name[]={"January","February","March","April","May","June","July","August","September","October","November","December"};

            //date.setText(day+" "+Month_name[y]+", "+year);
            text = day+" "+Month_name[y]+", "+year;
        }
        return  text;
    }


}
