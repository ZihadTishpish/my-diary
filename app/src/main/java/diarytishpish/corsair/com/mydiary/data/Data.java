package diarytishpish.corsair.com.mydiary.data;

/**
 * Created by Tiash on 7/10/2016.
 */
public class Data {
    private String title, description, cover,date,genre;
    private boolean favourite;
    
    public Data(String title, String description, String cover, String genre, String date, boolean favourite)
    {
        this.title = title;
        this.description = description;
        this.cover = cover;
        this.date = date;
        this.favourite = favourite;
        this.genre = genre;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
    public void setCover(String cover)
    {
        this.cover = cover;
    }
    public void setDescription(String description)
    {
        this.description=description;
    }
    public void setDate(String date)
    {
        this.date = date;
    }
    public void setFavourite(boolean favourite)
    {
        this.favourite = favourite;
    }
    public void setGenre(String genre)
    {
        this.genre = genre;
    }

    public String getTitle()
    {
        return title ;
    }
    public String getCover()
    {
        return cover;
    }
    public String getDescription()
    {
        return description;
    }
    public String getDate()
    {
        return date ;
    }
    public String getGenre()
    {
        return genre ;
    }
    public boolean getFavourite()
    {
        return favourite;
    }
}
