package diarytishpish.corsair.com.mydiary.adapters;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import diarytishpish.corsair.com.mydiary.R;
import diarytishpish.corsair.com.mydiary.data.Data;

public class FavouriteForMainPageAdapter extends RecyclerView.Adapter<FavouriteForMainPageAdapter.MyViewHolder> {

    private List<Data> AudioForMainPagesList;
    private Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        ImageView songCover;

        public MyViewHolder(View view)
        {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.genre);
            songCover = (ImageView) view.findViewById(R.id.audiocover);
        }
    }


    public FavouriteForMainPageAdapter(List<Data> AudioForMainPagesList, Context context)
    {
        this.AudioForMainPagesList = AudioForMainPagesList;
        this.context = context;
        //    Toast.makeText(context,"inside",Toast.LENGTH_LONG).show();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_item_card, parent, false);

        return new MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        String AudioForMainPage = AudioForMainPagesList.get(position).getTitle();
        holder.title.setText(AudioForMainPage);
        String date = AudioForMainPagesList.get(position).getDate();
        String string[]= date.split(" ");
        if (string.length==3)
        {
            String day = string[0];
            String month = string[1];
            String year = string[2];

            int y = Integer.parseInt(month);
            String Month_name[]={"January","February","March","April","May","June","July","August","September","October","November","December"};

            holder.count.setText(day+" "+Month_name[y]+", "+year);
        }
        int id = context.getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +AudioForMainPagesList.get(position).getCover() , null, null);
        holder.songCover.setImageResource(id);
        holder.itemView.animate().rotationY(-3).setDuration(250);
    }

    @Override
    public int getItemCount() {
        return AudioForMainPagesList.size();
    }
}
