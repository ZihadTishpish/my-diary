package diarytishpish.corsair.com.mydiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import diarytishpish.corsair.com.mydiary.R;
import diarytishpish.corsair.com.mydiary.data.Genre;

/**
 * Created by Tiash on 7/11/2016.
 */
public class StoryCoverAdapter extends RecyclerView.Adapter<StoryCoverAdapter.MyViewHolder> {

    private List<String> AudioForMainPagesList;
    private Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
    //    public TextView title, count;
        ImageView songCover;

        public MyViewHolder(View view)
        {
            super(view);
            songCover = (ImageView) view.findViewById(R.id.audiocover);
        }
    }

    public StoryCoverAdapter(List<String> AudioForMainPagesList, Context context)
    {
        this.AudioForMainPagesList = AudioForMainPagesList;
        this.context = context;
        //    Toast.makeText(context,"inside",Toast.LENGTH_LONG).show();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cover_choser_grid, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        String image = AudioForMainPagesList.get(position); //.getGenre();

      //  holder.count.setText(AudioForMainPagesList.get(position).getCount());
        int id = context.getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +image , null, null);
        holder.songCover.setImageResource(id);
    }

    @Override
    public int getItemCount() {
        return AudioForMainPagesList.size();
    }
}
