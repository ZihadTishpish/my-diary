package diarytishpish.corsair.com.mydiary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import java.util.ArrayList;
import java.util.List;

import diarytishpish.corsair.com.mydiary.data.AppSharedPreference;
import diarytishpish.corsair.com.mydiary.data.Data;
import diarytishpish.corsair.com.mydiary.data.Genre;

/**
 * Created by Tiash on 7/13/2016.
 */
public class CategoryData extends AppCompatActivity
{
    ListView list;
    CardView add_story;
    StoryAdapter storyAdapter;
    List<Data> storyList ;
    String category_name, category_cover;
    CollapsingToolbarLayout category_cover_image;
    AppBarLayout appBarLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_data_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        storyList = new ArrayList<>();
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        add_story = (CardView) findViewById(R.id.add_card);
        list = (ListView) findViewById(R.id.list);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);


        category_cover_image = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);


        getDataFromPreviousPage();
        getAllDataFromDatabase();

        storyAdapter = new StoryAdapter(this,storyList);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                asp.putCurrentDataToEdit("null","null","null","null",category_name,false);
                startActivity(new Intent(getApplicationContext(),AddStory.class));
            }
        });

        add_story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                asp.putCurrentDataToEdit("null","null","null","null","null",false);
                startActivity(new Intent(getApplicationContext(),AddStory.class));
            }
        });

        list.setAdapter(storyAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Data data = storyList.get(i);
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                asp.putCurrentDataToEdit(data.getTitle(),data.getDescription(),data.getCover(),data.getDate(),data.getGenre(),data.getFavourite());
                YoYo.with(Techniques.Swing).duration(500).interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                startActivity(new Intent(getApplicationContext(),ReadStory.class));
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).playOn(view);
            }
        });
        storyAdapter.notifyDataSetChanged();


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (category_cover_image.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(category_cover_image)) {
                    category_cover_image.setTitle(category_name);
                    add_story.setVisibility(View.VISIBLE);
                }
                else
                {
                    category_cover_image.setTitle(category_name);
                    add_story.setVisibility(View.GONE);
                }
            }
        });

    }

    private void getDataFromPreviousPage()
    {
        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        category_cover = asp.get_category_cover();
        category_name = asp.get_category_name();
        int id = getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +category_cover , null, null);
        category_cover_image.setBackgroundResource(id);
    }

    private void getAllDataFromDatabase()
    {
        //storyList = new ArrayList<>();
        SQLiteDatabase db = openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
        Cursor cursor=db.rawQuery("SELECT * FROM data", null);
        if (cursor.getCount()>0)
        {
            while(cursor.moveToNext())
            {
                // title VARCHAR,
                // description VARCHAR,
                // genre VARCHAR,
                // date VARCHAR,
                // favourite VARCHAR);");

                String title =cursor.getString(0);
                String description =cursor.getString(1);
                String genre = cursor.getString(2);
                String date =cursor.getString(3);
                String cover = cursor.getString(4);
                String fav = cursor.getString(5);
                if (genre.matches(category_name)) {
                    if (fav.matches("false"))
                        storyList.add(new Data(title, description, cover, genre, date, false));
                    else
                        storyList.add(new Data(title, description, cover, genre, date, true));
                }
               // card_item.add(new Genre(genre,count+" stories",image));
            }
        }
    }



    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(),ScrollingActivity.class));
    }

    public class StoryAdapter extends BaseAdapter
    {
        Activity activity;
        List<Data> topSong;
        ImageView like;

        public StoryAdapter(Activity activity, List<Data> topSong)
        {
            super();
            this.activity = activity;
            this.topSong = topSong;
            Toast.makeText(activity.getApplicationContext(),topSong.size()+" total",Toast.LENGTH_LONG).show();
        }

        @Override
        public int getCount() {
            return topSong.size();
        }

        @Override
        public Data getItem(int i) {
            return topSong.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }


        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            final Data data = topSong.get(i);
            LayoutInflater inflater=activity.getLayoutInflater();
            view=inflater.inflate(R.layout.story_card,null);
            ImageView cover = (ImageView) view.findViewById(R.id.storycover);
            TextView title = (TextView) view.findViewById(R.id.title);
            TextView date = (TextView) view.findViewById(R.id.date);
            like = (ImageView) view.findViewById(R.id.like);

            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data.getFavourite())
                    {
                       // like.setImageResource(R.mipmap.unlike);
                        data.setFavourite(false);
                        SQLiteDatabase db = activity.openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
                        db.execSQL("UPDATE data SET favourite ='false' WHERE title='"+data.getTitle()+"'");
                      //  storyList = new ArrayList<>();
                        //getAllDataFromDatabase();
                        storyAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        data.setFavourite(true);
                        SQLiteDatabase db = activity.openOrCreateDatabase("QuickDB", Context.MODE_PRIVATE, null);
                        db.execSQL("UPDATE data SET favourite ='true' WHERE title='"+data.getTitle()+"'");
                      //  storyList = new ArrayList<>();
                      //  getAllDataFromDatabase();
                        storyAdapter.notifyDataSetChanged();
                    }
                }
            });


            title.setText(data.getTitle());
            String string[]= data.getDate().split(" ");
            if (string.length==3)
            {
                String day = string[0];
                String month = string[1];
                String year = string[2];

                int y = Integer.parseInt(month);
                String Month_name[]={"January","February","March","April","May","June","July","August","September","October","November","December"};

                date.setText(day+" "+Month_name[y]+", "+year);
            }
            //date.setText(data.getDate());
            if (data.getFavourite())
                like.setImageResource(R.mipmap.fav);
            else
                like.setImageResource(R.mipmap.unlike);

            int id = activity.getResources().getIdentifier("diarytishpish.corsair.com.mydiary:mipmap/" +data.getCover() , null, null);
            cover.setImageResource(id);

            return view;
        }
    }

}
